-- | Mendefinisikan sebuah tree yang sederhana
data SimpleTree a = Leaf a | Branch (SimpleTree a) (SimpleTree a)

-- | Sebuah fungsi yang menjumlahkan semua elemen dalam tree integer
sumOnTree :: SimpleTree Int -> Int 
sumOnTree (Leaf x) = x
sumOnTree (Branch t1 t2) = sumOnTree t1 + sumOnTree t2

-- | Contoh kasar pengaplikasian sumOnTree
-- | sumOnTree (Branch (Branch (Leaf 5) (Leaf 4)) (Branch (Leaf 2) (Leaf 3)))

-- | Simple Expression Type
data SimpleExpr = CSimple Float | SimpleExpr :+ SimpleExpr | SimpleExpr :- SimpleExpr
    | SimpleExpr :* SimpleExpr | ExSimpleExprpr :/ SimpleExpr
    deriving Show

-- | Fungsi untuk mengevaluasi data Expr
evaluateSimple :: SimpleExpr -> Float
evaluateSimple (CSimple x) = x
evaluateSimple (e1 :+ e2) = evaluateSimple e1 + evaluateSimple e2
evaluateSimple (e1 :- e2) = evaluateSimple e1 - evaluateSimple e2
evaluateSimple (e1 :* e2) = evaluateSimple e1 * evaluateSimple e2
evaluateSimple (e1 :/ e2) = evaluateSimple e1 / evaluateSimple e2

-- | Complex Expression Type
data Expr = C Float 
          | Expr :+ Expr 
          | Expr :- Expr 
          | Expr :* Expr 
          | Expr :/ Expr
          | V String 
          | Let String Expr Expr
  deriving Show

-- | Subtitute Function
subst v0 e0 (V v1) = if (v0 == v1) then e0 else (V v1)
subst _ _ (C c) = (C c)
subst v0 e0 (e1 :+ e2) = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2) = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2) = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2) = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v0 e0 (subst v1 e1 e2)

evaluate :: Expr -> Float
evaluate (C x) = x
evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
evaluate (e1 :- e2) = evaluate e1 - evaluate e2
evaluate (e1 :* e2) = evaluate e1 * evaluate e2
evaluate (e1 :/ e2) = evaluate e1 / evaluate e2
evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
evaluate (V _) = 0.0