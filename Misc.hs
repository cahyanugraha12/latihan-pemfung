max' x y = if x > y then x else y

-- | More elegant max using guard and otherwise
max'' x y
    | x > y = x
    | otherwise = y

-- | Fibonacci infinite list from SCeLE
add [] []         = []
add (a:as) (b:bs) = (a+b) : (add as bs)

fibs  =  1 : 1 : add fibs (tail fibs)

-- | My Own Fibonacci
fibonacci = fibo [1,1,2]
    where fibo (x:xs) = [x] ++ fibo (xs ++ [last xs + last (init xs)])

-- | Function that flip or reverse argument
flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f a b = f b a

myFlip :: (a -> a -> a) -> a -> a -> a
myFlip f a b = f b a
