fpb x 0 = x
fpb x y
    | x < y = fpb y x
    | otherwise = fpb y (x `mod` y)

kpk x y = (x * y) `div` (fpb x y)