import Data.List

-- | Buatlah fungsi divisor yang menerima sebauh bilangan bulat n dan mengembalikan 
-- | list bilangan bulat positif yang membagi habis n
divisor n = [x | x <- [1..n], n `mod` x == 0]

-- | Buatlah definisi quick sort menggunakan list comprehension
quickSort :: [Int] -> [Int]
quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs, y <= x] ++ [x] ++ quickSort[y | y <- xs, y > x]

-- | Buatlah definisi infinite list untuk permutation
perm [] = [[]]
perm ls = [x:xs | x <- ls, xs <- perm(ls \\ [x])]

-- | Buatlah definisi untuk memberikan infinite list dari bilangan prima menerapkan 
-- | algoritma Sieve of Erastothenes.
primes = sieve [2 ..]
    where sieve (x:xs) = x : (sieve [z | z <- xs, z `mod` x /= 0])

-- | Buatlah definisi infinite list dari triple pythagoras. List tersebut terdiri dari element
-- | triple bilangan bulat positif yang mengikut persamaan pythagoras
triplePythagoras = [(x, y, z) | z <- [1..], y <- [1..z], x <- [1..y], (x * x) + (y * y) == (z * z)]