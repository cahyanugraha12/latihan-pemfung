toOne x = 1
myLength x = sum (map toOne x)

iter :: Int -> (a -> a) -> a -> a
iter 0 f a = a
iter n f a = iter (n-1) f (f a)

square x = x * x
nArray :: Int -> [Int]
nArray 0 = []
nArray n = n : nArray(n-1)

-- | How would you define the sum of the squares of the natural numbers 1
-- | to n using map and foldr?
sumOfSquares n = foldr (+) 0 (map square (nArray n))

-- | How does the function below behave?
mystery xs = foldr (++) [] (map sing xs)
    where 
        sing x = [x]
-- | Pertama, list yang menjadi parameter fungsi akan menjadi diubah menjadi list of list dimana inner listnya merupakan elemen dari list awal
-- | Lalu, foldr akan menggabungkan setiap elemen dari list of list menjadi sebuah list kembali
-- | Hasilnya identik dengan list awal