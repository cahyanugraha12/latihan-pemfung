data MaybeCahya x = NothingCahya | JustCahya x deriving Show

JustCahya x >>= f = f (f x)
NothingCahya >>= _ = NothingCahya

tambahLima x = JustCahya (x + 5)